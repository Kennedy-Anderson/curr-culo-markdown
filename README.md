# Kennedy Anderson Andrade Rocha
####  Brasileiro, solteiro, 20 anos
#### Endereço- Rua Rubem Valentim, 6, Village Âncora de Stella Maris casa 5,  
#### Stella Maris, Salvador
#### Telefone: (74)98803-8031 / E-mail: kennedyandrade98@gmail.com
>

| Objetivo |
| ------ |
| Conquistar emprego, ou estágio, na área de minha formação, técnico em informática, e do meu curso de graduação. Para adquirir experiência profissional na área. |

| Formação |
| ------ |
|  Ensino Fundamental – Concluído – Colégio Cassimira Maria Machado – Uibaí - BA |
|  Ensino Médio – Concluído – IFBA – Campus Irecê - BA |
|  Curso Técnico em Informática – Concluído – IFBA – Campus Irecê - BA |
|  Graduando em Ciência da Computação - Cursando - UFBA |

| Experiência Profissional |
| ------ |
|  19 de abril de 2018 a 31 de janeiro de 2019 – IFBA – Campus Irecê 
 Cargo: Estagiário no setor administrativo (Direção Geral) 
Principais atividades: Entrega e recebimento de documentos, digitação de documentos gerais, gestão de documentos |

| Qualificações  |
| ------ |
| Conhecimento em lógica de programação e linguagens de programação como C, C++, Java, HTML, JavaScript |




